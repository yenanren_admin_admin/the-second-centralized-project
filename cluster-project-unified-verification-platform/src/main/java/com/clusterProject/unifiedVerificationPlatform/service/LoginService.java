package com.clusterProject.unifiedVerificationPlatform.service;

import com.clusterProject.unifiedVerificationPlatform.entity.User;

public interface LoginService  {

    User usernameLogin(User user);

    User phoneEmailLogin(User user);
}
