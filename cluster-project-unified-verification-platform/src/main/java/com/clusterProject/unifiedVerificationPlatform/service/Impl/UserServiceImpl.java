package com.clusterProject.unifiedVerificationPlatform.service.Impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.clusterProject.unifiedVerificationPlatform.entity.User;
import com.clusterProject.unifiedVerificationPlatform.mapper.UserDAO;
import com.clusterProject.unifiedVerificationPlatform.service.UserService;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl extends ServiceImpl<UserDAO, User> implements UserService {
}
