package com.clusterProject.unifiedVerificationPlatform.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.clusterProject.unifiedVerificationPlatform.entity.Student;


public interface StudentService extends IService<Student> {
}