package com.clusterProject.unifiedVerificationPlatform.service.Impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.clusterProject.unifiedVerificationPlatform.entity.Student;
import com.clusterProject.unifiedVerificationPlatform.mapper.StudentDAO;
import com.clusterProject.unifiedVerificationPlatform.service.StudentService;
import org.springframework.stereotype.Service;

@Service
public class StudentServiceImpl extends ServiceImpl<StudentDAO, Student> implements StudentService {
}
