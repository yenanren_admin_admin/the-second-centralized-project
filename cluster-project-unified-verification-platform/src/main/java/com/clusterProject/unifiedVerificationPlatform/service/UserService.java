package com.clusterProject.unifiedVerificationPlatform.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.clusterProject.unifiedVerificationPlatform.entity.User;

import java.util.List;


public interface UserService extends IService<User> {

}