package com.clusterProject.unifiedVerificationPlatform.service;

public interface AsyncService {

    void testExecuteAsync();

    void delayDeleteUserCode(String getPhoneEmail);

}
