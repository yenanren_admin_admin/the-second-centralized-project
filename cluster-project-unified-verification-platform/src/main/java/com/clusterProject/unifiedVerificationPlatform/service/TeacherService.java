package com.clusterProject.unifiedVerificationPlatform.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.clusterProject.unifiedVerificationPlatform.entity.Teacher;

public interface TeacherService extends IService<Teacher> {
}