package com.clusterProject.unifiedVerificationPlatform.service.Impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.clusterProject.unifiedVerificationPlatform.entity.Teacher;
import com.clusterProject.unifiedVerificationPlatform.mapper.TeacherDAO;
import com.clusterProject.unifiedVerificationPlatform.service.TeacherService;
import org.springframework.stereotype.Service;

@Service
public class TeacherServiceImpl extends ServiceImpl<TeacherDAO, Teacher> implements TeacherService {
}
