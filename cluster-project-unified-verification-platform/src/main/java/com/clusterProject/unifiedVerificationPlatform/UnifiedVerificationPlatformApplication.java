package com.clusterProject.unifiedVerificationPlatform;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@MapperScan("com.clusterProject.unifiedVerificationPlatform.mapper")
@EnableAsync
public class UnifiedVerificationPlatformApplication {

    public static void main(String[] args) {
        SpringApplication.run(UnifiedVerificationPlatformApplication.class, args);
    }
}
