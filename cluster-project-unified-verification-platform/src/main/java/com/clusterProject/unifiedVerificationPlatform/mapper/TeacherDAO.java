package com.clusterProject.unifiedVerificationPlatform.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.clusterProject.unifiedVerificationPlatform.entity.Teacher;
import org.springframework.stereotype.Repository;

/**
 * TeacherDAO继承基类
 */
@Repository
public interface TeacherDAO extends BaseMapper<Teacher> {
}