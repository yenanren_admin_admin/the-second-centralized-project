package com.clusterProject.chatPlatform;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class chatPlatformApplication {
    public static void main(String[] args) {
        SpringApplication.run(chatPlatformApplication.class, args);
    }
}
